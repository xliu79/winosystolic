# Network yaml format

A example cam be found at config_yaml folder

## Convolution layer

    - type: 
        Required and must be conv 
         
    - top:
        Required. A string specifying output featuremap
        example:
        - top: conv1_out

    - bottom:
        Required. A string specifying input featuremap, if it is the first layer, it must be img
        example:
        - bottom: conv1_out

    - kernel_size:
        Required. A 2-element list as [kernel_height,kernel_width], currently kernel_height must equal kernel_width
        example:
        - kernel_size: [5,5]

    - out_channels:
        Required. An integer specifying output depth
        example:
        - out_channels: 6

    - weightfile:
        Optional. A string specifying the weight binary file name, the weight are stored in flattened order of [OD,ID,KH,KW] in float32 
        example:
        - weightfile: convnet.c1.weight.bin

    - biasfile: 
        Optional. A string specifying the bias binary file name, the bias are stored in flattened order of [OD] in float32 
        example:
        - biasfile: convnet.c1.bias.bin

    - stride:
        Optional. Default=1. 
        example
        - stride: 2
    
    - pad_size:
        Optional. Default=0.
        Can either be an integer or string False of string True. If specified as True, the pad_size will be intepreted as kernel_height/2. If specified as False, the pad size shall be intepreted as 0.
        example:
        - pad_size: 0
        - pad_size: True
    
    - groups:
        Optional. Default=1
        Integer as group number.
        example:
        - group: 1

    - in_channels:
        Optional & For validation. An integer specifying input depth. This can be filled for correctness validation.
        example:
        - in_channels: 32

    - in_dim:
        Optional & For validation. A list specifying [indepth, inheight, inwidth].
        - in_dim: [64, 224, 224]

    - out_dim:
        Optional & For validation. A list specifying [outdepth, outheight, outwidth].
        - out_dim: [64, 224, 224]

## Max Pooling layer

    - type: 
        Required and must be maxpool

    - top:
        Required. A string specifying output featuremap
        example:
        - top: maxpool1_out

    - bottom:
        Required. A string specifying input featuremap, if it is the first layer, it must be img
        example:
        - bottom: conv1_out 

    - kernel_size:
        Required. A 2-element list as [kernel_height,kernel_width], currently kernel_height must equal kernel_width
        example:
        - kernel_size: [5,5]

    - stride:
        Optional. Default=kernel_size. 
        example
        - stride: 2

    - pad_size:
        Optional. Default=0.
        Can either be an integer or string False of string True. If specified as True, the pad_size will be intepreted as kernel_height/2. If specified as False, the pad size shall be intepreted as 0.
        example:
        - pad_size: 0
        - pad_size: True

    - in_channels:
        Optional & For validation. An integer specifying input depth. This can be filled for correctness validation.
        example:
        - in_channels: 32

    - out_channels:
        Optional & For validation. An integer specifying output depth. This can be filled for correctness validation.
        example:
        - out_channels: 32

    - in_dim:
        Optional & For validation. A list specifying [indepth, inheight, inwidth].
        - in_dim: [64, 224, 224]

    - out_dim:
        Optional & For validation. A list specifying [outdepth, outheight, outwidth].
        - out_dim: [64, 224, 224]

## ReLU layer 

    - type: 
        Required and must be ReLU

    - top:
        Required. A string specifying output featuremap
        example:
        - top: relu1_out

    - bottom:
        Required. A string specifying input featuremap, if it is the first layer, it must be img
        example:
        - bottom: maxpool1_out 

    - in_channels:
        Optional & For validation. An integer specifying input depth. This can be filled for correctness validation.
        example:
        - in_channels: 32

    - out_channels:
        Optional & For validation. An integer specifying input depth. This can be filled for correctness validation.
        example:
        - out_channels: 32

    - in_dim:
        Optional & For validation. A list specifying [indepth, inheight, inwidth].
        - in_dim: [64, 224, 224]

    - out_dim:
        Optional & For validation. A list specifying [outdepth, outheight, outwidth].
        - out_dim: [64, 224, 224]

## Flatten layer

    - type: 
        Required and must be flatten

    - top:
        Required. A string specifying output featuremap
        example:
        - top: flatten_relu5_out 

    - bottom:
        Required. A string specifying input featuremap, if it is the first layer, it must be img
        example:
        - bottom: relu5_out

    - in_channels:
        Optional. An integer specifying input depth. This can be filled for correctness validation.
        example:
        - in_channels: 32

    - out_channels:
        Optional. An integer specifying input depth. This can be filled for correctness validation.
        example:
        - out_channels: 32

    - in_dim:
        Optional & For validation. A list specifying [indepth, inheight, inwidth].
        - in_dim: [32, 5, 5]

    - out_dim:
        Optional & For validation. A list specifying [outdepth, outheight, outwidth]. In such case, outheight and outwidth should be exactly 1
        - out_dim: [800, 1, 1]

## Linear Layer

    - type: 
        Required and must be linear

    - top:
        Required. A string specifying output featuremap
        example:
        - top: fc1_out

    - bottom:
        Required. A string specifying input featuremap, if it is the first layer, it must be img
        example:
        - bottom: flatten_relu5_out 

    - out_channels:
        Required. An integer specifying output depth
        example:
        - out_channels: 1024

    - weightfile:
        Optional. A string specifying the weight binary file name, the weight are stored in flattened order of [OD,ID] in float32 
        example:
        - weightfile: convnet.f6.weight.bin

    - biasfile: 
        Optional. A string specifying the bias binary file name, the bias are stored in flattened order of [OD] in float32 
        example:
        - biasfile: convnet.f6.bias.bin

    - in_channels:
        Optional. An integer specifying input depth. This can be filled for correctness validation.
        example:
        - in_channels: 800

## Residual Layer

    - type: 
        Required and must be residual

    - top:
        Required. A string specifying output featuremap
        example:
        - top: res1out

    - bottom:
        Required. A list of string specifying all the input feature map.
        example:
        - bottom: [res1a, res1b]