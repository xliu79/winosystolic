def genHWconfig(
    filename,
    WINO_DOMAIN_SIZE,
    INPUT_BUFFER_DEPTH_BITWIDTH,
    OUTPUT_BUFFER_DEPTH_BITWIDTH,
    WEIGHT_BUFFER_DEPTH_BITWIDTH,
    WINO_HEIGHT,
    WINO_WIDTH,
    INBUFFER_HEIGHT_BITWIDTH,
    INDEPTH_MINITILE_SIZE_BITWIDTH,
):
    fptr=open(filename,"w");
    fptr.write("#ifndef _WINO_HW_CONFIG_H_\n")
    fptr.write("#define _WINO_HW_CONFIG_H_\n\n")
    fptr.write("#define WINO_DOMAIN_SIZE "+str(WINO_DOMAIN_SIZE)+"\n")
    fptr.write("#define INPUT_BUFFER_DEPTH_BITWIDTH "+str(INPUT_BUFFER_DEPTH_BITWIDTH)+"\n")
    fptr.write("#define OUTPUT_BUFFER_DEPTH_BITWIDTH "+str(OUTPUT_BUFFER_DEPTH_BITWIDTH)+"\n")
    fptr.write("#define WEIGHT_BUFFER_DEPTH_BITWIDTH "+str(WEIGHT_BUFFER_DEPTH_BITWIDTH)+"\n")
    fptr.write("#define WINO_HEIGHT "+str(WINO_HEIGHT)+"\n")
    fptr.write("#define WINO_WIDTH "+str(WINO_WIDTH)+"\n")
    fptr.write("#define INBUFFER_HEIGHT_BITWIDTH "+str(INBUFFER_HEIGHT_BITWIDTH)+"\n")
    fptr.write("#define INDEPTH_MINITILE_SIZE_BITWIDTH "+str(INDEPTH_MINITILE_SIZE_BITWIDTH)+"\n")
    fptr.write("#endif\n\n")
    fptr.close()




genHWconfig(
    "src/wino_hw_config.h",
    4,
    12,
    10,
    10,
    8,
    7,
    3,
    2)
