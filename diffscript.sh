#!/bin/bash

# for ((var=0; var< wino_height-1;var++))
# do
#     echo diff -q weightstream${var}.txt weightstream${var}_gold.txt
#     diff -q weightstream${var}.txt weightstream${var}_gold.txt
# done

wino_height=4
wino_width=3


diff -q output1_test.txt  output1.txt
diff -q output2_test.txt  output2.txt
diff -q input_buffer_content_gold.txt  input_buffer_content.txt

for ((var=0; var< wino_height;var++))
do
    for ((j=0; j<wino_width;j++))
    do
        diff -q invector_${var}_${j}.txt invector_${var}_${j}_gold.txt
    done
done

for ((var=0; var< wino_height;var++))
do
    for ((j=0; j<wino_width;j++))
    do
        diff -q wvector_${var}_${j}.txt wvector_${var}_${j}_gold.txt
    done
done


for ((var=0; var< wino_height;var++))
do
    for ((j=0; j<wino_width;j++))
    do
        diff -q uvvector_${var}_${j}.txt uvvector_${var}_${j}_gold.txt
    done
done


for ((var=0; var< wino_height;var++))
do
    for ((j=0; j<wino_width;j++))
    do
        diff -q outvector_${var}_${j}.txt outvector_${var}_${j}_gold.txt
    done
done

diff -q outbuffer_gold.txt  outbuffer.txt


diff -q output1_test.txt  output1_hw.txt
diff -q output2_test.txt  output2_hw.txt