############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
############################################################
open_project wino_hls
set_top wino_systolic_top
add_files src/wino.cpp
add_files src/wino_macro.h
open_solution "solution1"
set_part {xczu9eg-ffvb1156-2-i}
create_clock -period 4 -name default
#source "./wino_hls/solution1/directives.tcl"
#csim_design -clean -O
remove_core Mul
config_core -latency 2 DSP48
config_schedule -disable_reduceDpCEfanout
csynth_design
#cosim_design -trace_level all
export_design -flow syn -rtl verilog -format ip_catalog
