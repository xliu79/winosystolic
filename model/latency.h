
//* TODO establish the frame work
typedef struct{
    int inheight;
    int inwidth;
    int indepth;
    int outheight;
    int outwidth;
    int outdepth;
    int kernel_size;
    int pad_szie;
    int stride;
} SoftwareInfo_t;


typedef struct{
    int wino_domain_size;
    int wino_domain_size_square;

    int input_buffer_depth_bitwidth;
    int input_buffer_depth;

    int output_buffer_depth_bitwidth;
    int output_buffer_depth;

    int weight_buffer_depth_bitwidth;
    int weight_buffer_depth;

    int wino_height;
    int wino_width;

    int inbuffer_width_bitwidth;
    int inbuffer_width;

    int inbuffer_height_bitwidth;
    int inbuffer_height;

    int indepth_minitile_size_bitwidth;
    int indepth_minitile_size;

    int weight_feed_number_per_port;
    int weight_feed_number_per_port_bitwidth;
} HardwareInfo_t;
