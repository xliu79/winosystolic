#include <string>
#include "../src/wino_macro.h"
#include "../src/wino_struct.h"
#include "../software/param.h"
#include "latency.h"
#include <cstring>
#include <stdio.h>
#include <cstdlib>
#include <iostream>

void print_tab(int number)
{
    for(int i=0;i<number;i++)
    {
        printf("\t");
    }
}


std::string itoa_str(int index)
{
    char result[30];
    sprintf(result,"%d",index);
    std::string retstr(result);
    return retstr;
}

class TaskSegInfo_t{
    public:
        float latency;
        float data_density;
        bool done;
        int sequence_idx;
        std::vector<TaskSegInfo_t*> deptask_list;
        std::string task_name;


        TaskSegInfo_t(float latency, float data_density, std::string task_name) 
        { 
            this->latency=latency;
            this->data_density=data_density;
            this->task_name=task_name;
            this->done=false;
        } 
        
        void add_dep_task_single( TaskSegInfo_t* task)
        {
            if(task==NULL) return;

            this->deptask_list.push_back(  task );
        
        }
};

void add_sequence_idx(std::vector<TaskSegInfo_t*> & task_sequnce)
{
    for(int i=0;i<task_sequnce.size();i++)
    {
        task_sequnce[i]->sequence_idx=i;
    }
}



bool assert_task_start(TaskSegInfo_t* task)
{
    for(int i=0;i< task->deptask_list.size();i++)
    {
        if(!task->deptask_list[i]->done) return false;
    }
    return true;
}


bool assert_task_end(
    std::vector<std::vector<TaskSegInfo_t*> > & task_sequence_list,
    std::vector<int> cur_task_idx_list
)
{
    for(int task_sequence_idx=0;task_sequence_idx<task_sequence_list.size();task_sequence_idx++)
    {

        if(task_sequence_list[task_sequence_idx].size()!=0 && !( task_sequence_list[task_sequence_idx].back()->done))
        {
            return false;
        }
    }

    return true;
}

void init_cur_task_idx_list(std::vector<int> & cur_task_idx_list, int size)
{
    for(int i=0;i<size;i++)
    {
        cur_task_idx_list.push_back(-1);
    }
}


void init_cur_task_remain_info_list(std::vector<TaskSegInfo_t*> &cur_task_remain_info_list, int size)
{
    for(int i=0;i<size;i++)
    {
        cur_task_remain_info_list.push_back(NULL);
    }
}

void update_cur_task_remain_info(
    std::vector<int> &cur_task_idx_list,
    std::vector<TaskSegInfo_t*> &cur_task_remain_info_list,
    std::vector<std::vector<TaskSegInfo_t*> > & task_sequence_lists
    )
{
    std::cout<<"Updating cur task remain info" <<std::endl;
    for(int i=0;i<cur_task_remain_info_list.size();i++)
    {
        if(cur_task_remain_info_list[i]==NULL || cur_task_remain_info_list[i]->done)
        {
            cur_task_remain_info_list[i]=NULL;
            int task_idx_to_start=cur_task_idx_list[i]+1;
            // std::cout<<"i "<<i<<std::endl;
            // std::cout<<"task_idx_to_start "<< task_idx_to_start<<std::endl;
            // std::cout<<"vector_size "<<task_sequence_lists[i].size()<<std::endl;
            // fflush(stdout);
            if(task_idx_to_start<task_sequence_lists[i].size() && assert_task_start(task_sequence_lists[i][task_idx_to_start]) )
            {
                cur_task_remain_info_list[i]= task_sequence_lists[i][task_idx_to_start];
                std::cout<<"\tTask sequence: "<<task_sequence_lists[i][task_idx_to_start]->task_name<<":"<<task_sequence_lists[i][task_idx_to_start]->sequence_idx<<" is loaded"<<std::endl;
                cur_task_idx_list[i]++;
            }
        }
    }
}


float latency_processing(
    std::vector<TaskSegInfo_t*> &cur_task_remain_info_list
    )
{

    // summarizing o

    float ret_latency;
    std::vector<float> scaled_latency;
    
    std::cout<<">>>latency_processing START"<<std::endl;
    for(int i=0;i< cur_task_remain_info_list.size();i++)
    {
        if(cur_task_remain_info_list[i]!=NULL)
        {
            std::cout<<cur_task_remain_info_list[i]->task_name<<":"<<cur_task_remain_info_list[i]->sequence_idx<<" | density:"<<cur_task_remain_info_list[i]->data_density<<std::endl;
            std::cout<<"\tlatency:"<<cur_task_remain_info_list[i]->latency<<" | done:"<<std::boolalpha<<cur_task_remain_info_list[i]->done<<std::endl;
        }
    }
    std::cout<<"------------------"<<std::endl;
    // getchar();

    bool has_zero_task_flag=false;
    for(int i=0;i< cur_task_remain_info_list.size();i++)
    {
        if( cur_task_remain_info_list[i]!=NULL)
        if( cur_task_remain_info_list[i]->latency==0 && cur_task_remain_info_list[i]->done==false)
        {
            cur_task_remain_info_list[i]->done=true;
            has_zero_task_flag=true;
        }
    }


    if(has_zero_task_flag)
    {
        ret_latency=0;
    }
    else
    {
        float density_total=0;
        for(int i=0;i< cur_task_remain_info_list.size();i++)
        {
            if(cur_task_remain_info_list[i]!=NULL)
            {
                density_total+=cur_task_remain_info_list[i]->data_density;
            }
        }
        float scale_factor=density_total>1?density_total:1;
        

        for(int i=0;i< cur_task_remain_info_list.size();i++)
        {
            if( cur_task_remain_info_list[i]==NULL)
            {
                            scaled_latency.push_back(0);
            }
            else if(cur_task_remain_info_list[i]->data_density==0)
            {
                scaled_latency.push_back(cur_task_remain_info_list[i]->latency);
            }
            else
            {
                scaled_latency.push_back(cur_task_remain_info_list[i]->latency*scale_factor);
            }
            
        }

        //find the min latency
        int min_scaled_latency_idx;
        float min_scaled_latency=1000000000000000000000000.0;

        for(int i=0;i< cur_task_remain_info_list.size();i++)
        {
            if( cur_task_remain_info_list[i]!=NULL)
            {
                if( scaled_latency[i]<min_scaled_latency)
                {
                    min_scaled_latency=scaled_latency[i];
                    min_scaled_latency_idx=i;
                }
            }
        }

        for(int i=0;i< cur_task_remain_info_list.size();i++)
        {
            if( cur_task_remain_info_list[i]!=NULL)
            {
                scaled_latency[i]-=min_scaled_latency;
                if(scaled_latency[i]==0)
                {
                    cur_task_remain_info_list[i]->done=true;
                    cur_task_remain_info_list[i]->latency=0;
                }
                else if(cur_task_remain_info_list[i]->data_density==0)
                {
                    cur_task_remain_info_list[i]->latency=scaled_latency[i];
                }
                else
                {
                    cur_task_remain_info_list[i]->latency=scaled_latency[i]/scale_factor;
                }
            }
        }  
        ret_latency=min_scaled_latency;   
    }



    
    for(int i=0;i< cur_task_remain_info_list.size();i++)
    {
        if(cur_task_remain_info_list[i]!=NULL)
        {
            std::cout<<cur_task_remain_info_list[i]->task_name<<":"<<cur_task_remain_info_list[i]->sequence_idx<<" | density:"<<cur_task_remain_info_list[i]->data_density<<std::endl;
            std::cout<<"\tlatency:"<<cur_task_remain_info_list[i]->latency<<" | done:"<<std::boolalpha<<cur_task_remain_info_list[i]->done<<std::endl;
        }
    }
    std::cout<<"<<<latency_processing END"<<std::endl;
    
    // getchar();



    return ret_latency;  
}



float summary_latency(std::vector<std::vector<TaskSegInfo_t*> > & task_sequence_list)
{
    std::cout<<"$$$$$$$$$$$$summary_latency START"<<std::endl;
    std::vector<int> cur_task_idx_list;
    std::vector<TaskSegInfo_t*> cur_task_remain_info_list;
    float latency=0;

    init_cur_task_idx_list(cur_task_idx_list,task_sequence_list.size());
    init_cur_task_remain_info_list(cur_task_remain_info_list,task_sequence_list.size());

    while(!assert_task_end(task_sequence_list,cur_task_idx_list))
    {
        update_cur_task_remain_info(cur_task_idx_list,cur_task_remain_info_list,task_sequence_list);
        latency+=latency_processing(cur_task_remain_info_list);
        std::cout<<"Accumulative latency "<<latency<<std::endl;
    }

    std::cout<<"$$$$$$$$$$$$summary_latency END"<<std::endl;
    return latency;
}




void print_latency_task_sequence(  std::vector<TaskSegInfo_t*>  task_sequence)
{
    for(int i=0;i<task_sequence.size();i++)
    {
        std::cout<<task_sequence[i]->task_name<<":"<<task_sequence[i]->sequence_idx<<std::endl;
        std::cout<<"task length: "<<task_sequence[i]->latency<<std::endl;
        std::cout<<"task density: "<<task_sequence[i]->data_density<<std::endl;
        std::cout<<"task done: "<<task_sequence[i]->done<<std::endl;
        std::cout<<"task dep:"<<std::endl;
        for(int j=0;j<task_sequence[i]->deptask_list.size();j++)
        {
            std::cout<<"\t"<<task_sequence[i]->deptask_list[j]->task_name<<":"<<task_sequence[i]->deptask_list[j]->sequence_idx<<std::endl;
        }
        std::cout<<std::endl;
    }
}




TaskSegInfo_t* weight_streamer_LAT(
    int overhead,
    int loop_weight_feed_bound,
    int index,
    std::string prefix
)
{
    float data_length=overhead+loop_weight_feed_bound;
    std::string task_name=prefix+":weight_streamer:"+itoa_str(index);
    TaskSegInfo_t* ret =new TaskSegInfo_t(data_length,0,task_name);
    return ret;
}

TaskSegInfo_t* weight_load_LAT(
    int overhead,
    int weightDDR_port_burst_length,
    int index,
    std::string prefix
)
{
    float data_length=overhead+weightDDR_port_burst_length;
    float data_density=weightDDR_port_burst_length/data_length;
    std::string task_name=prefix+":weight_load:"+itoa_str(index);
    TaskSegInfo_t* ret =new TaskSegInfo_t(data_length,data_density,task_name);
    return ret;
}

TaskSegInfo_t* weight_kernel_flag_LAT(
    int index,
    std::string prefix
)
{
    std::string task_name=prefix+":weight_kernel_flag:"+itoa_str(index);
    TaskSegInfo_t* ret =new TaskSegInfo_t(0,0,task_name);
    return ret;

}


void weight_kernel_latency(
    std::vector<TaskSegInfo_t*> &weight_load,
    std::vector<TaskSegInfo_t*> &weight_streammer,
    std::vector<TaskSegInfo_t*> &weight_kernel_flag,
    TaskSegInfo_t* weight_kernel_start_flag_task,
    int first_flag,
    bool last_flag,
    ConvDesc_t & conv_desc
)
{


    int load_data_size=CEIL_DIV(conv_desc.weightDDR_port_burst_length,16)*18;
    int load_overhead=32;

    int stream_overhead=32;


    if(first_flag)
    {
        TaskSegInfo_t* pre_overhead;
        TaskSegInfo_t* pre_data;
        pre_overhead=weight_load_LAT(load_overhead,0,-1,"pre-overhead"); 
        pre_data=weight_load_LAT(load_data_size,load_data_size,-1,"pre-data");
        pre_overhead->add_dep_task_single(weight_kernel_start_flag_task);
        pre_data->add_dep_task_single(pre_overhead);
        weight_load.push_back(pre_overhead);
        weight_load.push_back(pre_data);
    }
    else
    {
        TaskSegInfo_t* pre_skip;
        pre_skip=weight_load_LAT(4,0,-1,"pre-skip"); 
        pre_skip->add_dep_task_single(weight_kernel_start_flag_task);
        weight_load.push_back(pre_skip);
    }

    TaskSegInfo_t* first_iter_flag=weight_kernel_flag_LAT(-1,"pre");
    first_iter_flag->add_dep_task_single(weight_load.back());
    weight_kernel_flag.push_back(first_iter_flag);

    for(int i =0;i<conv_desc.weightDDR_burst_number;i++)
    {
        if(!last_flag || i!=conv_desc.weightDDR_burst_number-1)
        {
            TaskSegInfo_t* iter_load_overhead;
            TaskSegInfo_t* iter_load_data;
            iter_load_overhead=weight_load_LAT(load_overhead,0,i,"iter-overhead"); 
            iter_load_data=weight_load_LAT(load_data_size,load_data_size,i,"iter-data");
            iter_load_overhead->add_dep_task_single(weight_kernel_flag.back());
            iter_load_data->add_dep_task_single(iter_load_overhead);
            weight_load.push_back(iter_load_overhead);
            weight_load.push_back(iter_load_data);
        }
        else
        {
            TaskSegInfo_t* post_skip;
            post_skip=weight_load_LAT(4,0,-1,"post-skip"); 
            post_skip->add_dep_task_single(weight_kernel_flag.back());
            weight_load.push_back(post_skip);
        }
        


        TaskSegInfo_t* iter_streamer;
        iter_streamer=weight_streamer_LAT(stream_overhead,conv_desc.loop_weight_feed_bound,i,"iter");
        iter_streamer->add_dep_task_single(weight_kernel_flag.back());
        weight_streammer.push_back(iter_streamer);


        TaskSegInfo_t* iter_flag=weight_kernel_flag_LAT(i,"iter");
        iter_flag->add_dep_task_single(weight_load.back());
        iter_flag->add_dep_task_single(weight_streammer.back());
        weight_kernel_flag.push_back(iter_flag);
    }
}





void input_latency(
    std::vector<TaskSegInfo_t*> &input_load,
    std::vector<TaskSegInfo_t*> &input_flag,
    TaskSegInfo_t* depedent_task,
    int required_loaded_input_row_number,
    int input_load_burst_length,
    int new_start_input_row,
    bool clear_flag,
    bool skip_flag,
    ConvDesc_t & conv_desc
)
{

    TaskSegInfo_t* input_start_flag=new TaskSegInfo_t(14,0,"start:input_flag");
    input_start_flag->add_dep_task_single(depedent_task);
    input_flag.push_back(input_start_flag);

    if(skip_flag) {
        return;
    }
    
    float load_pre=38;
    float load_post=12;


    static int loaded_input_row_number;
    if(clear_flag)
        loaded_input_row_number = new_start_input_row;
    

    std::cout<<"required_loaded_input_row_number"<<required_loaded_input_row_number<<std::endl;

    while( required_loaded_input_row_number > loaded_input_row_number)
    {
        if(loaded_input_row_number>= conv_desc.inheight)
        {
            TaskSegInfo_t* input_skip=new TaskSegInfo_t(4,0,"skip:input_load");
            input_skip->add_dep_task_single(input_flag.back());
            input_load.push_back(input_skip);

            TaskSegInfo_t* input_iter_flag=new TaskSegInfo_t(0,0,"iter:input_flag-idx"+itoa_str(loaded_input_row_number));
            input_iter_flag->add_dep_task_single(input_skip);
            input_flag.push_back(input_iter_flag);

        }
        else
        {
            TaskSegInfo_t* input_iter_preoverhead=new TaskSegInfo_t(load_pre, 0, "iter-pre:input_load-idx"+itoa_str(loaded_input_row_number));
            input_iter_preoverhead->add_dep_task_single(input_flag.back());
            input_load.push_back(input_iter_preoverhead);

            TaskSegInfo_t* input_iter_data=new TaskSegInfo_t(
                                                        (float) CEIL_DIV(input_load_burst_length,16)*18, 
                                                        1, 
                                                        "iter-data:input_load-idx"+itoa_str(loaded_input_row_number));
            input_iter_data->add_dep_task_single(input_load.back());
            input_load.push_back(input_iter_data);    
            
            TaskSegInfo_t* input_iter_postoverhead=new TaskSegInfo_t(load_post, 0, "iter-post:input_load-idx"+itoa_str(loaded_input_row_number));
            input_iter_postoverhead->add_dep_task_single(input_iter_data);
            input_load.push_back(input_iter_postoverhead);

            TaskSegInfo_t* input_iter_flag=new TaskSegInfo_t(0,0,"iter:input_flag-idx"+itoa_str(loaded_input_row_number));
            input_iter_flag->add_dep_task_single(input_iter_postoverhead);
            input_flag.push_back(input_iter_flag);
        }
        loaded_input_row_number+=4;
    }
    loaded_input_row_number=required_loaded_input_row_number;
	
}


void wino_kernel_merge_row_latency(
    std::vector<TaskSegInfo_t*> &weight_load,
    std::vector<TaskSegInfo_t*> &weight_streamer,
    std::vector<TaskSegInfo_t*> &weight_kernel_flag,
    TaskSegInfo_t* depedent_task,
    bool first_flag,
    bool last_flag,
    ConvDesc_t &conv_desc
)
{

    TaskSegInfo_t* overhead_flag=weight_kernel_flag_LAT(-1,"start:weight-row:");
    overhead_flag->latency=4;

    overhead_flag->add_dep_task_single(depedent_task);
    weight_kernel_flag.push_back(overhead_flag);

    for(int i=0;i<conv_desc.merge_kernel_size;i+=3)
    {
            weight_kernel_latency(
                weight_load,
                weight_streamer,
                weight_kernel_flag,
                weight_kernel_flag.back(),
                first_flag,
                last_flag,
                conv_desc
            );  
    }
}


void input_compute_latency(
    std::vector<TaskSegInfo_t*> &input_load,
    std::vector<TaskSegInfo_t*> &input_flag,
    std::vector<TaskSegInfo_t*> &weight_load,
    std::vector<TaskSegInfo_t*> &weight_streamer,
    std::vector<TaskSegInfo_t*> &weight_kernel_flag,
    std::vector<TaskSegInfo_t*> &input_compute_flag,
    TaskSegInfo_t* depedent_task,
    int start_output_row,
    int next_start_row,
    bool first_input_flag,
    int first_flag,
    int last_flag,
    ConvDesc_t conv_desc
)
{
    
    int next_required_loaded_input_row_number = (next_start_row+conv_desc.out_rowstep)*conv_desc.stride - conv_desc.pad_size+2;
    int new_start_input_row = next_start_row*conv_desc.stride-conv_desc.pad_size;
    if(new_start_input_row<0) new_start_input_row=0;
    int curr_required_loaded_input_row_number = (start_output_row+conv_desc.out_rowstep)*conv_desc.stride - conv_desc.pad_size+2;

    input_latency(
    input_load,
    input_flag,
    depedent_task,
    next_required_loaded_input_row_number,
    conv_desc.input_load_burst_length,
    0,
    1,
    !first_input_flag,
    conv_desc);

    TaskSegInfo_t* input_compute_pre = new TaskSegInfo_t(0,0,"pre:input_compute_flag");
    input_compute_pre->add_dep_task_single(input_flag.back());
    input_compute_flag.push_back(input_compute_pre);
    if(first_input_flag) return;
    
    bool merge_clear_flag= (conv_desc.merge_kernel_size>3);
       
    std::cout<<"3"<<std::endl;
    fflush(stdout);  
    for(int row_offset=0; row_offset<conv_desc.merge_kernel_size; row_offset+=3)
    {
            int required_loaded_input_row_number;
            int skip_flag;
            int clear_flag;

            if( row_offset+3<conv_desc.merge_kernel_size )
            {
                required_loaded_input_row_number=curr_required_loaded_input_row_number+3+row_offset;
                skip_flag=0;
                clear_flag=0;
            }
            else
            {
                required_loaded_input_row_number=next_required_loaded_input_row_number;
                skip_flag=last_flag;
                clear_flag=merge_clear_flag;  
            }

            input_latency(
                input_load,
                input_flag,
                input_compute_flag.back(),
                required_loaded_input_row_number,
                conv_desc.input_load_burst_length,
                new_start_input_row,
                clear_flag,
                skip_flag,
                conv_desc);
            
            wino_kernel_merge_row_latency(
                weight_load,
                weight_streamer,
                weight_kernel_flag,
                input_compute_flag.back(),
                first_flag,
                last_flag,
                conv_desc
            );

            TaskSegInfo_t* input_compute_iter = new TaskSegInfo_t(0,0,"iter:input_compute_flag"+itoa_str(row_offset));
            input_compute_iter->add_dep_task_single(input_flag.back());
            input_compute_iter->add_dep_task_single(weight_kernel_flag.back());
            input_compute_flag.push_back(input_compute_iter);
    }
}

void write_LAT(
    std::vector<TaskSegInfo_t*> &write_output,
    TaskSegInfo_t* depedent_task,
    int pre_overhead,
    int data_length,
    int post_overhead,
    int index,
    std::string prefix
)
{
    TaskSegInfo_t* output_preoverhead=new TaskSegInfo_t((float) pre_overhead, 0, prefix+"-pre:input_load:"+itoa_str(index));
    output_preoverhead->add_dep_task_single(depedent_task);
    write_output.push_back(output_preoverhead);

    TaskSegInfo_t* output_data=new TaskSegInfo_t(
                                                (float) data_length, 
                                                1, 
                                                prefix+"-data:write_output:"+itoa_str(index));
    output_data->add_dep_task_single(output_preoverhead);
    write_output.push_back(output_data);    
    
    TaskSegInfo_t* output_postoverhead=new TaskSegInfo_t((float) post_overhead, 0, prefix+"iter-post:write_output:"+itoa_str(index));
    output_postoverhead->add_dep_task_single(output_data);
    write_output.push_back(output_postoverhead);
}

void write_output_DDR_latency(
    std::vector<TaskSegInfo_t*> &write_output,
    std::vector<TaskSegInfo_t*> &write_flag,
    TaskSegInfo_t* depedent_task,
    int start_row_idx,
    ConvDesc_t &conv_desc
)
{

    float write_output_preovehead=80;
    float data_length= CEIL_DIV(conv_desc.output_burst_length,16)*4+conv_desc.output_burst_length;
    float write_output_postoverhead=3;
    TaskSegInfo_t* write_flag_first=new TaskSegInfo_t(24,0,"pre:write_flag");
    write_flag_first->add_dep_task_single(depedent_task);
    write_flag.push_back(write_flag_first);
	if(start_row_idx<0) return;

    for(int i=0;i<conv_desc.wino_tile_number_in_out_rowstep;i++)
    {
        if(start_row_idx+i*conv_desc.wino_output_tile_size<conv_desc.outheight)
        {
            write_LAT(
                write_output,
                write_flag.back(),
                write_output_preovehead,
                data_length,
                write_output_postoverhead,
                i,
                "iter"
            );
            TaskSegInfo_t* write_flag_iter=new TaskSegInfo_t(0,0,"iter:write_flag:"+itoa_str(i));
            write_flag_iter->add_dep_task_single(write_output.back());
            write_flag.push_back(write_flag_iter);
        }
    }
}

void clear_task_seq(std::vector<std::vector<TaskSegInfo_t*> > &task_sequence)
{
    for(int i=0;i<task_sequence.size();i++)
    {
        while(task_sequence[i].size()!=0)
        {
            delete task_sequence[i].back();
            task_sequence[i].pop_back();
        }
    }
}
float overall_latency(
    ConvDesc_t conv_desc
)
{
    int latency=0;
    latency+=128;//load_param
    latency+=128;//load_bias
    
    std::vector<std::vector<TaskSegInfo_t*> > task_sequence_read(7);
    std::vector<std::vector<TaskSegInfo_t*> > task_sequence_write(2);

    std::vector<TaskSegInfo_t*> &input_load=task_sequence_read[0];
    std::vector<TaskSegInfo_t*> &input_flag=task_sequence_read[1];
    std::vector<TaskSegInfo_t*> &weight_load=task_sequence_read[2];
    std::vector<TaskSegInfo_t*> &weight_streamer=task_sequence_read[3];
    std::vector<TaskSegInfo_t*> &weight_kernel_flag=task_sequence_read[4];
    std::vector<TaskSegInfo_t*> &input_compute_flag=task_sequence_read[5];
    std::vector<TaskSegInfo_t*> &wino_flag=task_sequence_read[6];
       

    std::vector<TaskSegInfo_t*> &write_output=task_sequence_write[0];
    std::vector<TaskSegInfo_t*> &write_flag=task_sequence_write[1];


    std::cout<<"1"<<std::endl;
    fflush(stdout);  

    input_compute_latency(
    input_load,
    input_flag,
    weight_load,
    weight_streamer,
    weight_kernel_flag,
    input_compute_flag,
    NULL,
    0,0,1,0,0,
    conv_desc);

       
    std::cout<<"2"<<std::endl;
    fflush(stdout);  
    TaskSegInfo_t* wino_first=new TaskSegInfo_t(0,0,"pre:wino_flag");
    wino_first->add_dep_task_single(input_compute_flag.back());
    wino_flag.push_back(wino_first);

    latency+=summary_latency(task_sequence_read);

 

    int write_start_row= -conv_desc.out_rowstep;
    int next_start_row= conv_desc.out_rowstep;

    for( int compute_start_row =0; compute_start_row < conv_desc.outheight; compute_start_row+=conv_desc.out_rowstep)
    {
        clear_task_seq(task_sequence_read);
        clear_task_seq(task_sequence_write);
        int start_row_idx_minus_pad_size=compute_start_row-conv_desc.pad_size;
        
        input_compute_latency(
        input_load,
        input_flag,
        weight_load,
        weight_streamer,
        weight_kernel_flag,
        input_compute_flag,
        NULL,
        compute_start_row,
        next_start_row,
        0,
        compute_start_row==0,
        next_start_row >= conv_desc.outheight,
        conv_desc);

        write_output_DDR_latency(
        write_output,
        write_flag,
        NULL,
        write_start_row,
        conv_desc);

        TaskSegInfo_t* wino_iter=new TaskSegInfo_t(0,0,"iter:wino_flag");
        wino_iter->add_dep_task_single(input_compute_flag.back());
        // wino_iter->add_dep_task_single(write_flag.back());
        wino_flag.push_back(wino_iter);

        float read_latency=summary_latency(task_sequence_read);
        float write_latency=summary_latency(task_sequence_write);

        std::cout<<"read_latency "<<read_latency<<std::endl;
        std::cout<<"write_latency "<<write_latency<<std::endl;
        std::cout<<std::endl;
        latency+=write_latency>read_latency?write_latency:read_latency;
        write_start_row+=conv_desc.out_rowstep;
        next_start_row+=conv_desc.out_rowstep;
    }


    clear_task_seq(task_sequence_read);
    clear_task_seq(task_sequence_write);

    write_output_DDR_latency(
    write_output,
    write_flag,
    NULL,
    write_start_row,
    conv_desc);

    latency+=summary_latency(task_sequence_write);
    std::cout<<"latency cycle "<<latency<<std::endl;
    std::cout<<"latency time "<<latency*5<<std::endl;
    getchar();

    std::cout<<"done"<<std::endl;
    fflush(stdout);
    clear_task_seq(task_sequence_read);
    clear_task_seq(task_sequence_write);
    // for(int i=0;i<task_sequence.size();i++)
    // {
    //     add_sequence_idx(task_sequence[i]);
    // }
    // // print_latency_task_sequence(input_load);
    // latency=summary_latency(task_sequence);


}








int main(int argc, char** argv)
{
    int inheight=atoi( argv[1]);
    int inwidth=atoi( argv[2]);
    int indepth=atoi( argv[3]);

    int outheight=atoi( argv[4]);
    int outwidth=atoi( argv[5]);
    int outdepth=atoi( argv[6]);
    int kernelsize=atoi( argv[7]);
    int padsize=atoi( argv[8]);
    int stride=atoi( argv[9]);


    ConvDesc_t conv_desc;
    // process_element6x6( inheight,inwidth,indepth,
    //                     outheight,outwidth,outdepth,
    //                     kernelsize,1,padsize,1,
    //                     0,ALIGN(indepth,8),
    //                     0,ALIGN(outdepth,8),
    //                     conv_desc);
        
    std::cout<<"done"<<std::endl;
    fflush(stdout);    
 int latency=overall_latency( conv_desc);

 
    std::cout<<latency<<std::endl;
    //  compute_overall(
	// 	row_round,
	// 	write_length,
	// 	write_overhead,
	// 	merge_round,
	// 	read_length,
	// 	read_overhead,
	// 	load_round,
	// 	load_length,
	// 	compute_length,
	// 	load_overhead,
	// 	compute_overhead);
}


