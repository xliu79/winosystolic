
#include <ap_int.h>



int overhead_sim(int length)
{
	int j=0;
	for(int i=0;i<length;i++)
	{
		#pragma HLS pipeline
		j*=i;

	}
	return j;
}


void load(
			int overhead,
			int length,
			ap_uint<128>* mem,
			ap_uint<128> buffer[1024])
{
#pragma HLS interface m_axi port=mem
#pragma HLS inline off
	ap_uint<10> j=overhead_sim(overhead);
	ap_uint<128>* mem_offset=mem+j;
	for(int i=0;i<length;i++)
	{
#pragma HLS pipeline
		buffer[i]=mem_offset[i];
	}
}

template<int dummy>
void write(
			int overhead,
			int length,
			ap_uint<128>* mem,
			ap_uint<128> buffer[1024])
{

	ap_uint<10> j=overhead_sim(overhead);
	ap_uint<128>* mem_offset=mem+j;
	for(int i=0;i<length;i++)
	{
#pragma HLS pipeline
		mem_offset[i]=buffer[i];
	}
}

template<int dummy>
void compute(
		int overhead,
		int length,
		ap_uint<128> buffer_in[1024],
		ap_uint<128> buffer_weight[1024],
		ap_uint<128> buffer_out[1024]
)
{
#pragma HLS inline off
	ap_uint<10> j=overhead_sim(overhead);
	for(int i=0;i<length;i++)
	{
#pragma HLS pipeline
		ap_uint<10> buffer_addr=i;
		buffer_out[buffer_addr]+=buffer_weight[buffer_addr]*buffer_in[buffer_addr];
	}
}




void weight_compute(
		int load_round,
		int load_length,
		int compute_length,
		int load_overhead,
		int compute_overhead,
		ap_uint<128>* mem,
		ap_uint<128> buffer_in[1024],
		ap_uint<128> buffer_out[1024]
		)
{



	ap_uint<128> buffer_weight[1024];

	load(load_overhead,load_length,mem,buffer_weight);

	for(int i=0;i<load_round;i++){
#pragma HLS dependence intra false variable=buffer_weight
		load( load_overhead,load_length,mem,buffer_weight);
		compute<0>(compute_overhead,compute_length,buffer_in,buffer_weight,buffer_out);
	}
	compute<0>(compute_overhead,compute_length,buffer_in,buffer_weight,buffer_out);

}


//void compute_and_load(
//		int merge_round,
//		int read_length,
//		int read_overhead,
//		int load_round,
//		int load_length,
//		int compute_length,
//		int load_overhead,
//		int compute_overhead,
//		ap_uint<128>* in_mem,
//		ap_uint<128>* weight_mem,
//		ap_uint<128> buffer_out[1024]
//)
//{
//	ap_uint<128> buffer_in[1024];
//	load(read_overhead, read_length,buffer_in);
//	for(int i=0;i<merge_round;i++)
//	{
//		for
//	}
//}



void compute_and_load(
		int merge_round,
		int read_length,
		int read_overhead,
		int load_round,
		int load_length,
		int compute_length,
		int load_overhead,
		int compute_overhead,
		ap_uint<128>* in_mem,
		ap_uint<128>* weight_mem,
		ap_uint<128> buffer_out[1024]
)
{
#pragma HLS interface m_axi port=in_mem depth=65535
#pragma HLS interface m_axi port=weight_mem depth=65535
	ap_uint<128> buffer_in[1024];
	load(read_overhead, read_length,in_mem,buffer_in);
	for(int i=0;i<merge_round;i++)
	{
		#pragma HLS dependence intra false variable=buffer_in
		load(read_overhead, read_length,in_mem,buffer_in);
		weight_compute(load_round,load_length,compute_length,load_overhead,compute_overhead,weight_mem,buffer_in,buffer_out);
	}
	weight_compute(load_round,load_length,compute_length,load_overhead,compute_overhead,weight_mem,buffer_in,buffer_out);
}


#pragma SDS data zero_copy(in_mem[0:65535])
#pragma SDS data sys_port(in_mem:ps_e_S_AXI_HP0_FPD)
#pragma SDS data zero_copy(weight_mem[0:65535])
#pragma SDS data sys_port(weight_mem:ps_e_S_AXI_HP0_FPD)
#pragma SDS data zero_copy(out_mem[0:65535])
#pragma SDS data sys_port(out_mem:ps_e_S_AXI_HP0_FPD)

void compute_overall(
		int row_round,
		int write_length,
		int write_overhead,
		int merge_round,
		int read_length,
		int read_overhead,
		int load_round,
		int load_length,
		int compute_length,
		int load_overhead,
		int compute_overhead,
		ap_uint<128>* in_mem,
		ap_uint<128>* weight_mem,
		ap_uint<128>* out_mem)
{
	ap_uint<128> buffer_out0[1024];
	ap_uint<128> buffer_out1[1024];

	int pingpong=0;
	for(int i=0;i<row_round;i++)
	{
		if(pingpong=0)
		{
            compute_and_load(
            merge_round,
            read_length,
            read_overhead,
            load_round,
            load_length,
            compute_length,
            load_overhead,
            compute_overhead,
            in_mem,
            weight_mem,
            buffer_out0);

            write<0>(write_overhead,write_length,out_mem,buffer_out1);
        
            
		}
		else
		{
            compute_and_load(
            merge_round,
            read_length,
            read_overhead,
            load_round,
            load_length,
            compute_length,
            load_overhead,
            compute_overhead,
            in_mem,
            weight_mem,
            buffer_out0);

            write<0>(write_overhead,write_length,out_mem,buffer_out0);
		}
	}
}



